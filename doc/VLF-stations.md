# LF/VLF Stations that can be received with a sound card #

 F kHz |   callsign   | location         | country
-------|--------------|------------------|-----------
96.000 | ____________ | ________________ | *The stations below require a sound card with<br>a sampling frequency of at least 192 kHz.*
82.800 | MKL          | Northwood/Crimond | UK
81.000 | GYN2         | Inskip | UK
77.500 | [DCF77](https://fr.wikipedia.org/wiki/DCF77) | Mainflingen | Germany
73.600 | CFH          | Halifax | Canada
68.500 | BPC          | Henan | China
65.800 | FUE          | [Kerlouan](https://fr.wikipedia.org/wiki/Centre_de_transmission_de_Kerlouan) | France
62.600 | FUG          | Le Regine | France
60.000 | [JJY-60](https://en.wikipedia.org/wiki/JJY) | [Hagene-yama](https://jjy.nict.go.jp/LFstation/hagane/index-e.html) | Japan
60.000 | [WWVB](https://en.wikipedia.org/wiki/WWVB) | Fort Collins | Colorado USA
60.000 | [MSF](https://en.wikipedia.org/wiki/Time_from_NPL_(MSF)) | Anthorn | UK
51.950 | GYW1         | Crimond | UK
49.000 | SXA          | [Kato Souli](https://en.wikipedia.org/wiki/Kato_Souli_Naval_Transmission_Facility) | Greece
48.000 | ____________ | ________________ | *The stations below require a sound card with<br>a sampling frequency of at least 96 kHz.*
45.900 | NSY NSC?     | Niscemi | Sicily, Italy
40.750 | NAU          | [Aguada](https://en.wikipedia.org/wiki/Naval_Radio_Transmitter_Facility_Aguada) | Puerto Rico
40.400 | SRC.SAS      | Grimeton, Varberg | Sweden (same site as SAQ)
40.000 | [JJY40](https://en.wikipedia.org/wiki/JJY) | [Ohtakadoya-yama](https://jjy.nict.go.jp/LFstation/otakado/index-e.html) | Japan
37.500 | TFK.NRK      | [Grindavik](https://en.wikipedia.org/wiki/Naval_Radio_Transmitter_Facility_Grindavik) | Iceland
30.000 | ============ | ================ | *Separation of LF (above) and VLF (below) bands.*
26.700 | TBB          | [Bafa](https://en.wikipedia.org/wiki/Denizk%C3%B6y_VLF_transmitter) | Turkey
25.500 | [RJHnn](https://en.wikipedia.org/wiki/Beta_(time_signal)) | 5 locations | Russia
25.200 | NLM          | [LaMoure](https://en.wikipedia.org/wiki/Naval_Radio_Transmitter_Facility_LaMoure) | North Dakota, USA
25.100 | [RJHnn](https://en.wikipedia.org/wiki/Beta_(time_signal)) | 5 locations | Russia
25.000 | [RJHnn](https://en.wikipedia.org/wiki/Beta_(time_signal)) | 5 locations | Russia
24.800 | NLK          | [Jim Creek](https://en.wikipedia.org/wiki/Jim_Creek_Naval_Radio_Station) | Washington, USA
24.000 | NAA          | [Cutler](https://en.wikipedia.org/wiki/VLF_Transmitter_Cutler) | Maine, USA
24.000 | ____________ | ________________ | *The stations below require a sound card with<br>a sampling frequency of at least 48 kHz.*
23.400 | [DHO38](https://en.wikipedia.org/wiki/VLF_transmitter_DHO38) | Rhauderfehn | Germany
23.000 | [RJHnn](https://en.wikipedia.org/wiki/Beta_(time_signal)) | 5 locations | Russia
22.600 | [HWU](https://en.wikipedia.org/wiki/HWU_transmitter) | [Rosnay](https://fr.wikipedia.org/wiki/Centre_de_transmissions_de_la_Marine_nationale_de_Rosnay) | France
22.200 | NDT.JJI      | [Ebino](https://en.wikipedia.org/wiki/Ebino_VLF_transmitter) | Japan
22.100 | GQD          | [Skelton](https://en.wikipedia.org/wiki/Skelton_Transmitting_Station) | UK
22.050 | ____________ | ________________ | *The stations below require a sound card with<br>a sampling frequency of at least 44.1 kHz.*
21.750 | [HWU](https://en.wikipedia.org/wiki/HWU_transmitter) | Rosnay | France
21.750 | HWV          | Le blanc | France (NATO)
21.400 | NPM          | [Lualualei](https://en.wikipedia.org/wiki/Lualualei_VLF_transmitter) | Hawaii, USA
20.900 | FTA.FTA2     | [Sainte-Assise](https://fr.wikipedia.org/wiki/%C3%89metteur_de_Sainte-Assise) | France
20.760 | ICV          | Isola Di Tavolara | Sardinia, Italy (NATO)
20.600 | 3SB          | Datong | China
20.600 | 3SA          | Changde | China
20.500 | [RJHnn](https://en.wikipedia.org/wiki/Beta_(time_signal)) | | Russia
20.270 | ICV          | Isola Di Tavolara | Sardinia, Italy (NATO)
20.000 | ???          | South Pole | *USA*
19.800 | NWC          | [Exmouth](https://en.wikipedia.org/wiki/Naval_Communication_Station_Harold_E._Holt) | Australia
19.600 | GQD.GBR.GBZ  | [Anthorn](https://en.wikipedia.org/wiki/Anthorn_Radio_Station) | UK
19.200 | VTX4         | INS Kattabomman | India
19.100 | [HWU](https://en.wikipedia.org/wiki/HWU_transmitter) | [Rosnay](https://fr.wikipedia.org/wiki/Centre_de_transmissions_de_la_Marine_nationale_de_Rosnay) | France
18.300 | [HWU](https://en.wikipedia.org/wiki/HWU_transmitter) | [Rosnay](https://fr.wikipedia.org/wiki/Centre_de_transmissions_de_la_Marine_nationale_de_Rosnay) | France
18.200 | VTX3         | INS Kattabomman | India
18.100 | RDL          | location varies | Russian Navy
17.200 | [SAQ](https://fr.wikipedia.org/wiki/Station_radio_de_Grimeton) | [Grimeton, Varberg](https://whc.unesco.org/fr/list/1134/) | Sweden
17.000 | VTX2         | INS Kattabomman | India
16.400 | [JXN](https://en.wikipedia.org/wiki/Noviken_VLF_Transmitter) | Novik | Norway
16.300 | VTX1         | [INS Kattabomman](https://en.wikipedia.org/wiki/INS_Kattabomman) | India
15.750 | *your TV*    | *PAL M* | *analog TV horizontal scan*
15.734 | *your TV*    | *NTSC* | *analog TV horizontal scan*
15.625 | *your TV*    | *PAL/PAL N/SECAM* | *analog TV horizontal scan*
15.100 | [HWU](https://en.wikipedia.org/wiki/HWU_transmitter) | [Rosnay](https://fr.wikipedia.org/wiki/Centre_de_transmissions_de_la_Marine_nationale_de_Rosnay) | France
14.881 |              | F3, [RSDN-20](https://en.wikipedia.org/wiki/Alpha_(navigation)) | Russia
13.000 | VL3DEF       | Woodside | Australia
12.649 |              | F2, [RSDN-20](https://en.wikipedia.org/wiki/Alpha_(navigation)) | Russia
11.905 |              | F1, [RSDN-20](https://en.wikipedia.org/wiki/Alpha_(navigation)) | Russia
10.600 | 3SB          | Datong | China
&nbsp; 3.000 | ============ | ================ | *separation of VLF (above) and ULF (below) bands*
&nbsp; 1.000 | | | Frequencies below are not regulated by ITU: ] 1 kHz .. 0 [
&nbsp; 0.300 | ============ | ================ | *separation of ULF (above) and SLF (below) bands*
&nbsp; 0.082 | [ZEVS](https://fr.wikipedia.org/wiki/ZEVS_(%C3%A9metteur)) | Kola peninsula | Russia.
&nbsp; 0.076 | [SANGUINE](https://en.wikipedia.org/wiki/Project_Sanguine) | Clam Lake, Wisconsin.<br>  Republic, Michigan. | USA. Discontinued in 2005
&nbsp; 0.060 |  | *[Mains power](https://en.wikipedia.org/wiki/Mains_electricity_by_country)* | *North and South Americas*
&nbsp; 0.050 |  | *Mains power* | *Africa, Asia, Autralia, Europe*
&nbsp; 0.030 | ============ | ================ | *separation of SLF (above) and ELF (below) bands*

## About the stability of this list
Because of the huge infrastructure required for transmission antennas, **station location** is a stable information.

Similarly, because of the constraints on frequencies (antenna size, transmitter design, existing receiver fleets, ITU regulations), the **frequencies** used by a station are limited and stable.

However, only the station's [call sign prefix](https://en.wikipedia.org/wiki/ITU_prefix) is regulated by the ITU. This information is less stable and, for example, some stations may share the same call sign, or a station may use several call signs.

## No rules

Many VLF stations are operated by the military, so don't expect any information on schedules. Check the NATO or China or Russian *exercises*. Or wars...

Some stations (mainly time signals) transmit continuously, others transmit for a short period each day, and still others transmit very sporadically (e.g. SAQ 2 or 3 times a year). Some stations alternate between several frequencies (e.g. HWU).

## Other lists
- [sid station](https://sidstation.loudet.org/stations-list-en.xhtml) only active stations (used everyday for **S**udden **I**onospheric **D**isturbances monitoring)
- [mw list](https://www.mwlist.org/vlf.php)
- [vlf.it](http://www.vlf.it/trond2/list.html)
- [sigid wiki](https://www.sigidwiki.com/wiki/Category:VLF) see also [Artemis](https://aresvalley.com/) a signal database.
- [wikipedia](https://en.wikipedia.org/wiki/List_of_VLF-transmitters)


